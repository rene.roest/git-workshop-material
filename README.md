# Git

## Getting started

```shell
$ git config --global user.name "John Doe"
$ git config --global user.emil "johndoe@cohesion.nl"
$ ssh-keygen -t rsa -b 2048
```

## Git stages

![git](gitover.png "Git")

## Basics

### Local repo

Set up a new directory and make a local repository
```shell
$ mkdir git_workshop && cd git_workshop
$ git init
Initialized empty Git repository in /Users/bug/git_workshop/.git/
```
Create a README file and do a git status
```shell
$ vi README
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	README

nothing added to commit but untracked files present (use "git add" to track)
```
### Adding files
Now let's add the README file to git
```shell
$ git add README
$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

	new file:   README
```
### Committing
Let's commit the file
```shell
$ git commit -m "initial commit"
[master (root-commit) 4985fe2] initial commit
 1 file changed, 1 insertion(+)
 create mode 100644 README
```
### Viewing/making changes
Now do a modification on the README
```shell
$ vi README
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   README

no changes added to commit (use "git add" and/or "git commit -a")
```
Now we can let git tell us what the differences are 'diff'
```shell
$ git diff
diff --git a/README b/README
index d2f87c3..c8581e7 100644
--- a/README
+++ b/README
@@ -1 +1,3 @@
 This is my README
+added this modification
+
```
We will revisit git diff later for some more advanced examples.
In the meantime let's make some more changes, including adding a new file
```shell
$ git add README
$ vi index.txt
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   README

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	index.txt

$ git add index.txt
$ git commit
[master 7a21514] - updated README - added index.txt
 1 file changed, 2 insertions(+)
```
### View history
Let's view the current commit history
```shell
$ git log
commit 7a21514bfae012cd900f32bbc8674551fae70787 (HEAD -> master)
Author: René Roest <r.roest@cohesion.nl>
Date:   Tue May 18 14:38:47 2021 +0200

    - updated README
    - added index.txt

commit 4985fe2b126b919d65e6cf4a217f087232ed3e7c
Author: René Roest <r.roest@cohesion.nl>
Date:   Tue May 18 14:27:58 2021 +0200

    initial commit 
```
let's see this  with actual changes as well
```shell
$ git log -p
commit 49d510a9970a142e3aab1bdf0be40a670a07a0c6 (HEAD -> master)
Author: René Roest <r.roest@cohesion.nl.nl>
Date:   Tue May 18 15:53:02 2021 +0200

    - updated README
    - added index.txt

diff --git a/README b/README
index 3296884..8490aea 100644
--- a/README
+++ b/README
@@ -1 +1,3 @@
 This is the README
+added this modification
+
diff --git a/index.txt b/index.txt
new file mode 100644
index 0000000..c3d940d
--- /dev/null
+++ b/index.txt
@@ -0,0 +1 @@
+index file

commit 6b525b39d64fdf11ea259eed7de1859b85eea954
Author: René Roest <r.roest@cohesion.nl.nl>
Date:   Tue May 18 15:50:33 2021 +0200

    initial commit

diff --git a/README b/README
new file mode 100644
index 0000000..3296884
--- /dev/null
+++ b/README
@@ -0,0 +1 @@
+This is the README+
```
### Removing a file
let's delete a file
```shell
$ rm index.txt
$ git rm index.txt
rm 'index.txt'
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	deleted:    index.txt

$ git commit -m "deleted index"
[master adf174f] deleted index
 1 file changed, 1 deletion(-)
 delete mode 100644 index.txt
```
## Undoing things

### Amending commit message
We are not happy with the last commit message, we can change it using *--amend*

**Warning**
Never amend commit message if they have already been pushed to remote
```shell
$ git commit --amend -m "deleted index, because it is not used"
[master a4a2fb8] deleted index, because it is not used
 Date: Tue May 18 16:01:20 2021 +0200
 1 file changed, 1 deletion(-)
 delete mode 100644 index.txt
$ git log
commit a4a2fb8591fd9228c7033807ebb217afc42dfceb (HEAD -> master)
Author: René Roest <r.roest@cohesion.nl.nl>
Date:   Tue May 18 16:01:20 2021 +0200

    deleted index, because it is not used

commit 49d510a9970a142e3aab1bdf0be40a670a07a0c6
Author: René Roest <r.roest@cohesion.nl>
Date:   Tue May 18 15:53:02 2021 +0200

    - updated README
    - added index.txt

commit 6b525b39d64fdf11ea259eed7de1859b85eea954
Author: René Roest <r.roest@cohesion.nl>
Date:   Tue May 18 15:50:33 2021 +0200

    initial commit
```
### Unstage a file
Unstaging means moving a file from staging area back to working directory/tree
```shell
$ vi README
$ git add README
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   README

$ git reset HEAD README
Unstaged changes after reset:
M	README
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   README

no changes added to commit (use "git add" and/or "git commit -a"
```
### unmodify a file
we unstaged the file but the file is still there with the changes
we would really like to get rid of these changes
```shell
$ git checkout -- README
$ git status
On branch master
nothing to commit, working tree clean
```
## Recap what happened so far
![what happened](git-visual-hist1.png  "what happened")

## Working with remotes
Create a (personal) blank repository
```shell
$ git remote add origin git@gitlab.com:rene.roest/git-workshop.git
$ git push origin -u master
Enumerating objects: 9, done.
Counting objects: 100% (9/9), done.
Delta compression using up to 8 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (9/9), 787 bytes | 787.00 KiB/s, done.
Total 9 (delta 0), reused 0 (delta 0)
To gitlab.com:rene.roest/git-workshop.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
```
Refresh the page in gitlab, it has all your changes now

## Working with branches

### Make a branch
```shell
$ git branch testing
```
![make branch](git-make-branch.png  "make branch")
```shell
$ git log --oneline --decorate
a4a2fb8 (HEAD -> master, origin/master, testing) deleted index, because it is not used
49d510a - updated README - added index.txt
6b525b3 initial commit
```
### Checkout branch
```shell
$ git checkout testing
Switched to branch 'testing'
```
![checkout branch](git-checkout-branch.png  "checkout  branch")

### Branch make change
Let's make a change in the testing branch
```shell
$ vi test.sh
$ git add test.sh
$ git commit -m "added a test"
[testing ffd5a5e] added a test
 1 file changed, 1 insertion(+)
 create mode 100644 test.sh
```
![change branch](git-branch-change.png  "change  branch")

### Branch, diverging
Let's go back to master branch and make a change to the README
```shell
$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
$ vi README
$ git add README
$ git commit -m "updated README, description"
[master c1b534b] updated README, description
 1 file changed, 1 insertion(+)
```
![diverge  branch](git-branch-diverge.png  "diverge branch")
```shell
$ git log --oneline --decorate --graph --all
* c1b534b (HEAD -> master) updated README, description
| * ffd5a5e (testing) added a test
|/
* a4a2fb8 (origin/master) deleted index, because it is not used
* 49d510a - updated README - added index.txt
* 6b525b3 initial commit
```

### Branch, merging
Next we're going to merge our changes from testing into master branch
```shell
$ git merge testing
Merge made by the 'recursive' strategy.
 test.sh | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 test.sh
$ git branch -d testing
Deleted branch testing (was ffd5a5e).
```
![merge branch](git-branch-merge.png  "merge  branch")
```shell
$ git log --oneline --decorate --graph --all
*   63a4afd (HEAD -> master) Merge branch 'testing'
|\
| * ffd5a5e added a test
* | c1b534b updated README, description
|/
* a4a2fb8 (origin/master) deleted index, because it is not used
* 49d510a - updated README - added index.txt
* 6b525b3 initial commit
```
Go ahead and push the changes to remote.
```shell
$ git push origin
Enumerating objects: 10, done.
Counting objects: 100% (10/10), done.
Delta compression using up to 8 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (8/8), 799 bytes | 799.00 KiB/s, done.
Total 8 (delta 1), reused 0 (delta 0)
To gitlab.com:rene.roest/git-workshop.git
   a4a2fb8..63a4afd  master -> master
```

## Rebase
in the previous merge example we saw the had a divergent history, this is not
something you might want, you can use rebase to keep a linear history.

Let's make a new branch fix-test, make a change. we will be using the
shorthand *checkout -b* to both create the branch and check it out. 
```shell
$ git checkout -b fix-test
Switched to a new branch 'fix-test'
$ vi test.sh
$ git add test.sh
$ git commit -m "updated test"
[fix-test 75b693e] updated test
 1 file changed, 1 insertion(+)
```
Now switch back to master and make a change to README
```shell
$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
$ vi README
$ git add README
$ git commit -m "Updated README"
[master 3e57e3c] Updated README
 1 file changed, 1 insertion(+)
```
as we can see we diverge again
![diverge  branch](git-branch-diverge-2.png  "diverge branch")
```shell
$ git log --oneline --decorate --graph --all
* 3e57e3c (HEAD -> master) Updated README
| * 75b693e (fix-test) updated test
|/
*   63a4afd (origin/master, origin/HEAD) Merge branch 'testing'
|\
| * ffd5a5e added a test
* | c1b534b updated README, description
|/
* a4a2fb8 deleted index, because it is not used
* 49d510a - updated README - added index.txt
* 6b525b3 initial commit
```
Now let's switch back to the fix-test and rebase master branch onto it.
```shell
$ git checkout fix-test
Switched to branch 'fix-test'
$ git rebase master
First, rewinding head to replay your work on top of it...
Applying: updated test
```
Next we can merge fix-test into master, note that the merge is fast-forward.
```shell
$ git checkout master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
$ git merge fix-test
Updating 3e57e3c..113d337
Fast-forward
 test.sh | 1 +
 1 file changed, 1 insertion(+)
$ git branch -d fix-test
Deleted branch fix-test (was 113d337).
```
So what actually happened? basically the change introduced in 75b693e updated test
was played onto change 3e57e3c Update README and made into a new 'commit' 113d337 
![rebased branch](git-branch-rebased.png  "rebased branch")
```shell
$ git log --oneline --decorate --graph --all
* 113d337 (HEAD -> master) updated test
* 3e57e3c Updated README
*   63a4afd (origin/master, origin/HEAD) Merge branch 'testing'
|\
| * ffd5a5e added a test
* | c1b534b updated README, description
|/
* a4a2fb8 deleted index, because it is not used
* 49d510a - updated README - added index.txt
* 6b525b3 initial commit
```
As we can see if we have a neat linear history now.

**WARNING**
Do not rebase commits that exist outside your repository and that people may have based work on.
<br/>If you follow that guideline, you’ll be fine. If you don’t, people will hate you, and you’ll be scorned by friends and family.

Let's push the changes to remote
```shell
$ git push origin
Enumerating objects: 9, done.
Counting objects: 100% (9/9), done.
Delta compression using up to 8 threads
Compressing objects: 100% (5/5), done.
Writing objects: 100% (6/6), 593 bytes | 593.00 KiB/s, done.
Total 6 (delta 1), reused 0 (delta 0)
To gitlab.com:rene.roest/git-workshop.git
   63a4afd..113d337  master -> master
```
### Rebase squash commits
Sometimes when you are working on a branch you may want to combine
several commits into a single one this is called squashing.

let's  make a new banch and add some commits.
```shell
$ git checkout -b  my-feature
Switched to a new branch 'my-feature'
$ vi README
$ git add README
$ git commit -m "added more content to README"
[my-feature 7286466] added more content to README
 1 file changed, 2 insertions(+)
$ vi README
$ git add README
$ git commit -m "fixed typo"
[my-feature 216b8ce] fixed typo
 1 file changed, 1 insertion(+), 1 deletion(-)
$ vi README
$ git add README
$ git commit -m "added content to README"
[my-feature 993fd60] added content to README
 1 file changed, 2 insertions(+), 1 deletion(-)
$ git log --oneline --decorate --graph --all
* 993fd60 (HEAD -> my-feature) added content to README
* 216b8ce fixed typo
* 7286466 added more content to README
* 113d337 (origin/master, origin/HEAD, master) updated test
* 3e57e3c Updated README
*   63a4afd Merge branch 'testing'
|\
| * ffd5a5e added a test
* | c1b534b updated README, description
|/
* a4a2fb8 deleted index, because it is not used
* 49d510a - updated README - added index.txt
* 6b525b3 initial commit 
```
As you can see we have added 3 commits, let's squash it into a single one.
(from HEAD till 3 commits)
```shell
$ git rebase -i HEAD~3
```
a editor (vi) sessions opens:
```vi
pick 7286466 added more content to README
pick 216b8ce fixed typo
pick 993fd60 added content to README

# Rebase 113d337..993fd60 onto 113d337 (3 commands)
#
# Commands:
# p, pick <commit> = use commit
# r, reword <commit> = use commit, but edit the commit message
# e, edit <commit> = use commit, but stop for amending
# s, squash <commit> = use commit, but meld into previous commit
# f, fixup <commit> = like "squash", but discard this commit's log message
# x, exec <command> = run command (the rest of the line) using shell
# b, break = stop here (continue rebase later with 'git rebase --continue')
# d, drop <commit> = remove commit
# l, label <label> = label current HEAD with a name
# t, reset <label> = reset HEAD to a label
# m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
# .       create a merge commit using the original merge commit's
# .       message (or the oneline, if no original merge commit was
# .       specified). Use -c <commit> to reword the commit message.
#
# These lines can be re-ordered; they are executed from top to bottom.
#
# If you remove a line here THAT COMMIT WILL BE LOST.
#
# However, if you remove everything, the rebase will be aborted.
#
# Note that empty commits are commented out
```
There are a lot more options to interactive rebase, but we will focus on squash only.
pick commits you want to squash and change pick to squash
```vi
pick 7286466 added more content to README
squash 216b8ce fixed typo
squash 993fd60 added content to README

# Rebase 113d337..993fd60 onto 113d337 (3 commands)
#
# Commands:
# p, pick <commit> = use commit
# r, reword <commit> = use commit, but edit the commit message
# e, edit <commit> = use commit, but stop for amending
# s, squash <commit> = use commit, but meld into previous commit
# f, fixup <commit> = like "squash", but discard this commit's log message
# x, exec <command> = run command (the rest of the line) using shell
# b, break = stop here (continue rebase later with 'git rebase --continue')
# d, drop <commit> = remove commit
# l, label <label> = label current HEAD with a name
# t, reset <label> = reset HEAD to a label
# m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
# .       create a merge commit using the original merge commit's
# .       message (or the oneline, if no original merge commit was
# .       specified). Use -c <commit> to reword the commit message.
#
# These lines can be re-ordered; they are executed from top to bottom.
#
# If you remove a line here THAT COMMIT WILL BE LOST.
#
# However, if you remove everything, the rebase will be aborted.
#
# Note that empty commits are commented out
```
exit editor when done and it will ask now for the commit message
```vi
# This is a combination of 3 commits.
# This is the 1st commit message:

added more content to README

# This is the commit message #2:

fixed typo

# This is the commit message #3:

added content to README

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# Date:      Wed May 19 10:22:38 2021 +0200
#
# interactive rebase in progress; onto 113d337
# Last commands done (3 commands done):
#    squash 216b8ce fixed typo
#    squash 993fd60 added content to README
# No commands remaining.
# You are currently rebasing branch 'my-feature' on '113d337'.
#
# Changes to be committed:
#       modified:   README
#
```
Go ahead and change the commit message
```vi
Added content to README
# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# Date:      Wed May 19 10:22:38 2021 +0200
#
# interactive rebase in progress; onto 113d337
# Last commands done (3 commands done):
#    squash 216b8ce fixed typo
#    squash 993fd60 added content to README
# No commands remaining.
# You are currently rebasing branch 'my-feature' on '113d337'.
#
# Changes to be committed:
#       modified:   README
#
```
exit the editor.
```shell
[detached HEAD 9c4b2ad] Added content to README
 Date: Wed May 19 10:22:38 2021 +0200
 1 file changed, 3 insertions(+)
Successfully rebased and updated refs/heads/my-feature.
$ git log --oneline --decorate --graph --all
* 9c4b2ad (HEAD -> my-feature) Added content to README
* 113d337 (origin/master, origin/HEAD, master) updated test
* 3e57e3c Updated README
*   63a4afd Merge branch 'testing'
|\
| * ffd5a5e added a test
* | c1b534b updated README, description
|/
* a4a2fb8 deleted index, because it is not used
* 49d510a - updated README - added index.txt
* 6b525b3 initial commit
```
as you can see we just have a single commit now.

## Cherry-picking
We'll stay onto the my-feature branch and add some more commits
```shell
$ vi test.sh
$ git add test.sh
$ git commit -m "fixed test failing"
[my-feature 4773453] fixed test failing
 1 file changed, 1 insertion(+), 1 deletion(-)
$ vi README
$ git add README
$ git commit -m "described test cases in README"
[my-feature 366cb21] described test cases in README
 1 file changed, 1 insertion(+), 1 deletion(-)
 ```
Now we have a fix for test, but feature is not yet completed. we would
like to have this fix applied now to master without merging our feature changes.
let's see how thing are looking now first:
![cherry before](git-cherry-before.png  "cherry before")
```shell
* 366cb21 (HEAD -> my-feature) described test cases in README
* 4773453 fixed test failing
* 9c4b2ad Added content to README
* 113d337 (origin/master, origin/HEAD, master) updated test
* 3e57e3c Updated README
*   63a4afd Merge branch 'testing'
|\
| * ffd5a5e added a test
* | c1b534b updated README, description
|/
* a4a2fb8 deleted index, because it is not used
* 49d510a - updated README - added index.txt
* 6b525b3 initial commit
```
We can see the fix is in commit 4773453, let's switch to master and cherry-pick
that commit
```shell
$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
$ git cherry-pick 4773453
[master cd32097] fixed test failing
 Date: Wed May 19 10:46:16 2021 +0200
 1 file changed, 1 insertion(+), 1 deletion(-)
``` 
![cherry after](git-cherry-after.png "cherry after") 
```shell
$ git log --oneline --decorate --graph --all
* cd32097 (HEAD -> master) fixed test failing
| * 366cb21 (my-feature) described test cases in README
| * 4773453 fixed test failing
| * 9c4b2ad Added content to README
|/
* 113d337 (origin/master, origin/HEAD) updated test
* 3e57e3c Updated README
*   63a4afd Merge branch 'testing'
|\
| * ffd5a5e added a test
* | c1b534b updated README, description
|/
* a4a2fb8 deleted index, because it is not used
* 49d510a - updated README - added index.txt
* 6b525b3 initial committhe 
```
## Stashing

Let's go back to my-feature and make a change, but don't commit it.
```shell
$ git checkout my-feature
Switched to branch 'my-feature'
$ vi README
$ git status
On branch my-feature
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   README

no changes added to commit (use "git add" and/or "git commit -a")
```
Now there is an emergency fix required, but we are not ready with our feature
so we are are not willing to commit to it.
```shell
$ git checkout master
error: Your local changes to the following files would be overwritten by checkout:
	README
Please commit your changes or stash them before you switch branches.
Aborting
$ git branch
  master
* my-feature
```
as you can see we cannot change these branch, and we are not wiling to commit just yet
git stash can help:
```shell
$ git stash
Saved working directory and index state WIP on my-feature: 366cb21 described test cases in README
$ git checkout master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
$ git checkout -b hotfix
Switched to a new branch 'hotfix'
$ ls
README	test.sh
$ vi fix
$ git add fix
$ git commit -m "wrote fix"
[hotfix 4d4968e] wrote fix
 1 file changed, 1 insertion(+)
 create mode 100644 fix
$ git checkout master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
$ git merge hotfix
Updating cd32097..4d4968e
Fast-forward
 fix | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 fix
$ git branch -d hotfix
Deleted branch hotfix (was 4d4968e).
```
We have made the fix and merged it into master, not let's get back to our feature
and finish it.
```shell
$ git checkout my-feature
Switched to branch 'my-feature'
$ git stash pop
On branch my-feature
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   README

no changes added to commit (use "git add" and/or "git commit -a")
Dropped refs/stash@{0} (52207138dee6a80aae6a463a5ae62a0255854940)
$ vi README
$ git add README
$ git commit -m "added a feature"
[my-feature 8fa41e7] added a feature
 1 file changed, 1 insertion(+)
```

## Finishing up
Let's rebase master onto my-feature and merge it into master.
```shell
$ git rebase master
First, rewinding head to replay your work on top of it...
Applying: Added content to README
Applying: described test cases in README
Applying: added a feature
$ git checkout master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)
$ git merge my-feature
Updating 4d4968e..7dd732a
Fast-forward
 README | 4 ++++
 1 file changed, 4 insertions(+)
$ git branch -d my-feature
Deleted branch my-feature (was 7dd732a).
$ git push origin
Enumerating objects: 18, done.
Counting objects: 100% (18/18), done.
Delta compression using up to 8 threads
Compressing objects: 100% (13/13), done.
Writing objects: 100% (15/15), 1.50 KiB | 766.00 KiB/s, done.
Total 15 (delta 3), reused 0 (delta 0)
To gitlab.com:rene.roest/git-workshop.git
   113d337..7dd732a  master -> master
```
Have a look yourself how our commit history looks like till now
## Next time undoing things